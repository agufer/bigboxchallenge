import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import { helpers } from './helpers/helpers'
import router from './router'

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import '@mdi/font/css/materialdesignicons.css'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const vuetify = createVuetify({
    components,
    directives,
    icons: {
        defaultSet: 'mdi',
    },
    theme: {
        themes: {
          light: {
            dark: false,
            colors: {
              text: '#464646',
              alterText: '#666666',
              primary: '#FF4C3E',
            }
          },
        },
    },
})

const app = createApp(App)
app.config.globalProperties.$helpers = helpers;
app.use(router).use(vuetify)

app.mount('#app')

