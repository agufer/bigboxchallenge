import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/actividades',
      name: 'actividades',
      component: () => import('../views/ActividadesView.vue')
    },
    {
      path: '/actividad/:id',
      name: 'actividad',
      component: () => import('../views/ActividadView.vue')
    }
  ]
})

router.afterEach((to, from) => {
  if (from.name === to.name && from.params.id !== to.params.id) {
    router.go(0)
  }
})

export default router
