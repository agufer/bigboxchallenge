interface ActividadBaseBodyInterface {
    activity_type: string,
    bigbox_points: number,
    biglife_instance_id: number,
    id: number,
    order: number,
    participants: number,
    points: number,
    price: number,
    proxylogicaldeletemodel_ptr_id: number,
    title: string,
    visible: boolean
}
interface ActividadDetalleServer extends ActividadBaseBodyInterface {
    activity: string,
}
export interface ActividadDetalleParsed extends ActividadBaseBodyInterface {
    activity: DetalleActividadElementInterface
}

export interface ResponseActividadesAPI {
    [key: number]: ActividadDetalleServer
}

interface DetalleActividadElementInterface {
    activity_hash: string,
    activity_type: string,
    benefits: string,
    category: string,
    description: string,
    id: number,
    image: string[],
    is_remote: boolean,
    know_what: string,
    locations: LocationInterface[],
    market_price: number,
    name: string,
    participants: number,
    partner: string,
    rating: number,
    show_new: boolean,
    small_print: string,
    subtitle: string,
    tip: string,
    title: string
}

interface LocationInterface {
    address: string,
    delivery_in_all_country: false,
    lat: string,
    lng: string,
    phone: string,
    province: string
}