import axios from 'axios';

/**
 * ApiController class, has get and post functions.
 */
class ApiController {
    baseUrl: string;
    timeOut: number;

    constructor() {
        this.baseUrl = `${window.location.origin}/`;
        this.timeOut = 5000
    }

    /**
     * Makes an external get.
     * @param endpoint
     * @param successCallback
     * @param errorCallback
     */
    get(endpoint: string, successCallback: Function, errorCallback: Function | null = null) {
        return axios.get(endpoint, {timeout: this.timeOut})
        .then(response => {
            if (typeof successCallback === 'function') {
                successCallback(response.data);
            }
            return response.data
        })
        .catch(error => {
            console.debug("Fetch error: ", error.message)
            if (typeof errorCallback === 'function') {
                errorCallback(error);
            }
        })
        .finally(() => {})
    }
}

export const apiController = new ApiController()