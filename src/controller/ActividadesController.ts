import { apiController } from "./ApiController";
import type { ActividadDetalleParsed } from "./ExternalTypes";
/**
 * ApiController class, has get and post functions.
 */
class ActividadesController {
    constructor() {}

    /**
     * gets actividades by page
     * @param page
     * @param successCallback
     * @param errorCallback
     */
    getActividades(page: number ,successCallback: Function, errorCallback: Function | null = null) {
        apiController.get('https://www.frontchallenge.bigbox.com.ar/activity?_page='+ page +'&_limit=9', (response: any) => {
            for (const element of response) {
                element.activity = JSON.parse(element.activity)
            }

            const array: ActividadDetalleParsed[] = response
            successCallback(array)
        } , errorCallback)
    }

    /**
     * gets actividad by id
     * @param id id key de la actividad deseada
     * @param successCallback
     * @param errorCallback
     */
    async getActividad(id: number ,successCallback: Function , errorCallback: Function | null = null) {
        const element = await this.getActividadEndpoint(id, errorCallback)
        const actividadesSimilares = await this.calculateActividadesRelacionadas(id, element.activity_type, errorCallback)
        successCallback({
            element: element,
            actividadesSimilares: actividadesSimilares
        })
    }

    getActividadEndpoint(id: number, errorCallback: Function | null = null) {
        return apiController.get('https://www.frontchallenge.bigbox.com.ar/activity/' + id, (response: any) => {
            response.activity = JSON.parse(response.activity)
        } , errorCallback)
    }

    async calculateActividadesRelacionadas(id: number,activityType: string, errorCallback: Function | null = null) {
        const response = await apiController.get('https://www.frontchallenge.bigbox.com.ar/activity?_page=1&_limit=100', () => {} , errorCallback)
        const actividadesSimilares = []
        for (const element of response) {
            if (actividadesSimilares.length === 4) {
                return actividadesSimilares
            }
            element.activity = JSON.parse(element.activity)
            if (element.activity_type === activityType && element.id != id) {
                actividadesSimilares.push(element)
            }
        }
        return actividadesSimilares
    }
}

export const actividadesController = new ActividadesController()